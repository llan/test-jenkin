package com.bandwidth.billing.basic;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Calendar;
import java.util.Properties;
import java.util.Random;


/**
 * Basic test producer that sends time to the test topic on a
 * random interval between 0-10 secs.
 *
 * Built off of quickstart maven archetype
 */
public class KafkaProducerRandomTime {
    public static void main(String[] args) {
        System.out.println("Yay for useless kafka stuff");

        Properties props = new Properties();

        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        Random rand = new Random();
        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        for(int i = 0; i < 100; i++) {
            producer.send(new ProducerRecord<String, String>("test", "randomKey", "Randome Value " + i + ": " + Calendar.getInstance().getTime() ));

            try {
                Thread.sleep(rand.nextInt(10000)+1);
            } catch(InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }


        producer.close();
    }
}
